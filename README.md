# Octopodes

A simple web app for monitoring and controlling multiple Octoprint instances from a single page.

Has a scrolling twin pane layout with printers on the left and your files on the right. Just click a file to select it and click "Print selected" on a printer to start.
![](https://i.imgur.com/sb9Rp8j.png)

# Issues
I'm gonna be straight up, the code is super jank and probably buggy. I was mainly concerned with getting something that worked for me, with as little time & effort spent as possible. Many parts of the code are probably gonna look very strange like the time estimations or the file selecting. Currently I don't recommend using this unless you have some programming experience as you will probably need to make adjustments and correct assumptions I've made.

With that out of the way lets get to the good parts!

# Features
- The main one: **Control & monitor multiple Octoprint instances from a single page!**
- More lightweight than Octoprint's own interface. Loads much faster and is quicker to respond.
- Compatible with static webservers; no server side requirements or a database needed as it's just HTML and JavaScript.
- Supports themes; comes with a built-in minimal light theme and a fancy dark theme (that's enabled by default).
- Support for PSU control (requires [this Octoprint plugin](https://github.com/kantlivelong/OctoPrint-PSUControl)). It will also automatically turn on a printer when starting a job and then wait for the printer to connect before starting it (PSU control plugin can be configured to autoconnect on power on).

# Installation
1. Put `index.html`, `config.js`, `fa-solid-900.woff2`, and the `themes/` directory with it's contents into a directory on your webserver.
2. Edit `config.js`

# ToDo
If the code quality hasn't scared you off yet ;P contributions are very welcome, especially on anything listed below:
- Add more sorting options for files list (currently it's just alphanumeric order and no "folder first" option).
- Add file uploading support.
- Add feature to change some Octoprint settings.
- Add support for logging in with user/pass (currently requires guests to have full permissions).
- Improve ui for phones and portrait displays (e.g. make sections collapseable & add touch support for separator/resizer).
- ~~Support a light theme and make theming easier.~~ (Done)
