// Config file for Octopodes

// Path to theme css to use, comment out to just use minimal built-in theme
const THEME = 'themes/dark.css'

// I use the same api key for all my octoprint instances, if you don't you'll need to replace each APIKEY in the devices list below
const APIKEY = 'YOUR API KEY HERE' 

// List of values to display as buttons for quickly setting the hotend (tool0) & bed temps, like api key you can set them all the same here or override them per printer below
const TEMPS = {
	tool0: [220, 205, 160],
	bed: [60, 50]
}

// The addr for each device needs to point to the web interface (including port number) of that instance octoprint interface
const devices = [
	{name: 'PRINTER_ONE', addr: 'http://example.home:5000', apikey: APIKEY, temps: TEMPS},
	{name: 'PRINTER_TWO', addr: 'http://', apikey: APIKEY, temps: TEMPS},
]

// We have to pick which Octoprint instance to get the file list from as only one is supported currently
const fileHost = devices[devices.length-1]
